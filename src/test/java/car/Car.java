package car;

public class Car {

    protected String name; //zmiana modyfikatora dostępu dla pól matki na protected na potrzeby dziedziczenia!!!
    protected String color;
    protected String engineSize;

    public Car(String name, String color, String engineSize) {
        this.name = name;
        this.color = color;
        this.engineSize = engineSize;
    }

    //jeśli chcemy zabronić generowania danych pól to jest usuwamy

//    public String getName() {
//        return name; //zwraca dane dla pola
//    }
//
//    public void setName(String name) {
//        this.name = name; //przyjmuje jeden parametr niczego nie zwraca i ustawia nam dane pole
//    }
//
//    public String getColor() {
//        return color;
//    }
//
//    public void setColor(String color) {
//        this.color = color;
//    }
//
//    public String getEngineSize() {
//        return engineSize;
//    }
//
//    public void setEngineSize(String engineSize) {
//        this.engineSize = engineSize;
//    }
}
