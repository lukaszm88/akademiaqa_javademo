package car;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Streamy {
    public static void main(String[] args) {

        //Streamy - strumienie wartosci. uzywamy przy kolekcjach, lista gdy chcemy je filtrowac, zmienia, przechodzić przez nie
        //Składają się z metod:
        //pośrednich - zwrają nowe streamy
        //filter - filtrowanie elementów
        //map - przekształcanie elementów
        //terminalnych - wykonywane jako ostatnie (wartość lub zebrać wszystkie wartości do kolekcji)
        //forEach
        //min | max | sum
        //anyMatch | allMatch | nonMatch
        //count
        //collect -  zbieranie elementów

        List<String> names = new ArrayList<String>();
        names.add("Bartek");
        names.add("Antek");
        names.add("Ela");
        names.add("Zuzia");
        names.add("Ania");
        names.add("Franek");

//        for (String name : names) {
//            if (name.startsWith("A")) {
//                System.out.println(name);
//            }
//        }

        names.stream()
                .filter(name -> name.startsWith("A"))
                //.forEach(name -> System.out.println(name));
                .forEach (System.out::println);

        long a = names.stream()
                .filter(name -> name.startsWith("A"))
                //.forEach(name -> System.out.println(name));
                .count();

        System.out.println(a);

        names.stream()
                .map(name -> name.toUpperCase())
                .forEach (System.out::println);

        names.stream()
                .map(name -> name + " jakiś text")
                .forEach (System.out::println);


        boolean anyMatchEndsWithK = names.stream()
                .anyMatch(name -> name.endsWith("g"));

        System.out.println(anyMatchEndsWithK);

        boolean allMatchEndsWithK = names.stream()
                .anyMatch(name -> name.endsWith("k"));

        System.out.println(anyMatchEndsWithK);

        List<String> namesStartWithA = names.stream()
                .filter(name -> name.startsWith("A"))
                .filter(name -> !name.endsWith("k"))
                .collect(Collectors.toList());
        //.forEach(name -> System.out.println(name));
        namesStartWithA.stream()
        .forEach (System.out::println);

        //f(x)= x*x
        //x-> x*x  //lambda

        //Lista obiektów

        List<User> users = new ArrayList<>();
        users.add(new User("Bartek","czarny","bartek@example.com", 17, true));
        users.add(new User("Tomek","niebieski","tomek@example.pl", 42, false));
        users.add(new User("Basia","czerwony","basia@example.pl", 31, true));
        users.add(new User("Ania","zielony","ania@example.eu", 12, false));
        users.add(new User("Zuzia","rozowy","zuzia@example.com", 23, true));

        System.out.println(" ");
        System.out.println("uzytkownicy których adres email konczą się na .pl");
        users.stream()
                .filter(user -> user.getEmail().endsWith(".pl"))
                .forEach (System.out::println);

        System.out.println(" ");
        System.out.println("uzytkownicy którzy nie lubią lodów");
        users.stream()
                .filter(user -> user.getLikeIceCream().equals(false))
                .forEach (System.out::println);

        System.out.println(" ");
        System.out.println("dorosli userzy");
        users.stream()
                .filter(user -> user.getAge()>18)
                .forEach (System.out::println);

        System.out.println(" ");
        System.out.println("najmlodsi userzy");
        User theYoungestUsers = users.stream()
                .min(Comparator.comparing(user -> user.getAge()))
                .get();
        theYoungestUsers.introduceYourSelf();
    }
}
