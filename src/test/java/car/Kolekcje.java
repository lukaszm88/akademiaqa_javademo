package car;

import model.User;

import java.util.*;

public class Kolekcje {
    public static void main(String[] args) {

        //kolekcje przechowują elementy tego samego typu (zbiór elementów)

        //Lista - zwykła lista element1, element2, element3

        List<String> namesList = new ArrayList<String>();
        namesList.add("Bartek"); //index 0
        namesList.add("Antek"); //index 1
        namesList.add("Ela"); //index 2
        namesList.add("Zuzia"); //index 3
        namesList.add("Ania"); //index 4

        System.out.println(namesList.size());
        System.out.println(namesList.get(0));

        //Pętla
        //potrzebujemy uzyc indexu
        for (int i = 0; i < namesList.size(); i++) {
            System.out.println(namesList.get(i));
        }

        //nie potrzebujemy uzyc indexu
        for (String name : namesList) { //dla kazdego imienia z listy imion, wez to zrób
            System.out.println(name);
        }

        List<User> users = new ArrayList<User>();
        users.add(new User("Bartek", "Testowy", "bartek@example.com", 21, true));
        users.add(new User("Bartek1", "Testowy", "bartek1@example.com", 32, false));
        users.add(new User("Bartek1", "Testowy", "bartek2@example.com", 67, true));

        for (User user : users) {
            user.introduceYourSelf();
        }


        //MAPA - przechowuje klucz (unikalny) i wartosc

        Map<Integer, String> namesMap = new HashMap<Integer, String>();
        namesMap.put(1, "Lukasz");
        namesMap.put(2, "Ola");
        namesMap.put(3, "Magda");

        System.out.println(namesMap.get(2));

        for (Map.Entry<Integer, String> entry : namesMap.entrySet()) {
            System.out.println(entry.getValue());
        }


        //SET - Tylko unikalne wartosci

        Set<String> namesSet = new HashSet<String>();
        namesSet.add("Karol");
        namesSet.add("Karol"); //ignoruje ten wpis
        namesSet.add("Karol"); //ignoruje ten wpis
        namesSet.add("Gerwazy");
        namesSet.add("Tadeusz");
        System.out.println(namesSet.size());

        for (String name : namesSet) {
            System.out.println(name);
        }
    }
}
