package car;

public class FamilyCar extends Car {
    // Nie powtarzajmy kodu - niech dziedziczy pola po klasie Car
//    private String name;
//    private String color;
//    private String engineSize;
    //private String carType = "Sports Car";
    private String carType; //ustawimy je sobie w kontruktorze

    //tworzymy konstruktor
    public FamilyCar(String name, String color, String engineSize, String carType) {
        super(name, color, engineSize); //jest to wywołanie konstruktora z klasy matki
        this.carType = carType;
    }

    //najpierw podaje parametery dla pól klasy matki, a potem na pola z klasy

    public void describeCar() {
        System.out.println("Car name: " + name + " color: " + color + " engine size: " + engineSize + " car type: " + carType);
    }
}
