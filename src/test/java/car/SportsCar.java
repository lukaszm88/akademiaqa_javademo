package car;

public class SportsCar extends Car {
// Nie powtarzajmy kodu - niech dziedziczy pola po klasie Car
//    private String name;
//    private String color;
//    private String engineSize;

    //is a - coś jest czymś - dziedziczenie
    //has a - posiada coś (kompozycja) budujemy jedną klasę z innych klas (obiektów)

    //private String carType = "Family Car";
    private String carType; //ustawimy je sobie w kontruktorze
    public SportsCar(String name, String color, String engineSize, String carType) {
        super(name, color, engineSize);
        this.carType = carType;
    }

    public void describeCar() {
        System.out.println("Car name: " + name + " color: " + color + " engine size: " + engineSize + " car type: " + carType);
    }
}
