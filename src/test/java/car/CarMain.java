package car;

public class CarMain {
    public static void main(String[] args) {
//        Car mazda = new Car("Mazda 3", "silver", "2.3");
//        System.out.println(mazda.getName());
//        System.out.println(mazda.getColor());
//        System.out.println(mazda.getEngineSize());
//
//        Car hyundai = new Car("","black", "2.0");
//        hyundai.setName("i40");
//        System.out.println(hyundai.getName());
//        System.out.println(hyundai.getColor());
//        System.out.println(hyundai.getEngineSize());

        FamilyCar familyCar = new FamilyCar("Hyundai i40", "black", "2.0", "Family Car");
        SportsCar sportsCar = new SportsCar("Hyundai i30 N Performance", "blue", "2.0", "Sports Car");

        familyCar.describeCar();
        sportsCar.describeCar();
    }
}

