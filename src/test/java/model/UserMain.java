package model;

public class UserMain {

    public static void main(String[] args) {
        //typ nazwa = wartosc;
//        User user1 = new User(); //konstruktor
////rozwiązanie nieoptymalne
//        user1.firstName = "Lukasz";
//        user1.lastName = "Testowy";
//        user1.email = "lukasz@example.com";
//        user1.age = 32;
//        user1.likeIceCream = true;
//        System.out.println("User name: " + user1.firstName + " "
//                + user1.lastName +
//                " email: " + user1.email +
//                " age: " + user1.age +
//                " likeIceCream: " + user1.likeIceCream);

        User user1 = new User("Lukasz", "Testowy", "lukasz@example.com", 32, true);
//        System.out.println(user1.firstName + " " + user1.lastName + " " + user1.email);
        User user2 = new User("Anna", "Testowa", "anna@example.com", 18, true);
        User user3 = new User("Jarek", "Testowy", "jarek@example.com", 21, false);
//        System.out.println(user2.firstName + " " + user2.lastName + " " + user2.email);
//        System.out.println(user3.firstName + " " + user3.lastName + " " + user3.email);

        user1.introduceYourSelf();
        Boolean adult = user1.isAdult();
        System.out.println(adult);

        user2.introduceYourSelf();
        //user2.isAdult(); - niepotrzebna jeśli chcemy od razu wyświetlić
        System.out.println(user2.isAdult());

        user3.introduceYourSelf();
        user3.isAdult();
        user3.metodaZParametrem("jeden");
        user3.metodaZParametrami("jeden", "dwa");

        //przeciążone metody
        User user4 = new User("Basia", "Testowy", "basia@example.com", 36, false);
        user4.metodaZParametrem("jeden", "dwa");

        //Modyfikatory dostępu
        User user5 = new User("Basia", "Testowy", "basia@example.com", 36, false);
       // user5.age = 20; //jak zmienie modyfikator dostępu dla age to już nie mogę się dostać do tego pola

       // System.out.println(user5.age); // zadziałał domyślny modyfikator dostępu

    }
}
