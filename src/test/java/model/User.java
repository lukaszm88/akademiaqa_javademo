package model;

public class User {

    //1. Pola obiektu - opisują z czego składa się obiekt
    private String firstName;
    private String lastName;
    private String email;
    private int age; //dodaje modyfikator dostępu i nie pozwalam go zmieniać
    private Boolean likeIceCream;


    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", likeIceCream=" + likeIceCream +
                '}';
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public Boolean getLikeIceCream() {
        return likeIceCream;
    }


    //5. modyfikatory dostepu
    //a) public - dostępne wszędzie
    //b) private - dostępny tylko w klasie
    //c) protected - dostepny tylko w klasach które będą dziedziczyć po tej klasie i pakiecie

    // jak nie określimy modyfikatora dostępu to zadziała domyślny modyfikator dostępu
    // dobra praktyka jest taka aby pola były ustawione jako prywatne - dostęp w odrębie naszej klasy natomiast metody publiczne


    //2. Konstruktor - specjalna metoda która powoduje, że możemy budować obiekty - podajemy dane z któego będzie skłądał się obiekt i go tworzymy
    //a) Konstruktor domyślny - gdy nie podamy pól
    //b) Konstruktor zdefiniowany - taka sama nazwa jak nazwa klasy

    public User(String firstName, String lastName, String email, int age, boolean likeIceCream) { //-takie same parametry jak pola
        this.firstName = firstName;
        this.lastName = lastName;  //do zmiennej przypisuje wartość
        this.email = email;
        this.age = age;
        this.likeIceCream = likeIceCream;
    }


    //3. Metody - opisują co ten obiekt będzie mógł zrobić
    //void - jeśli chcemy tylko wyświetlić
    //boolean - jeśli chcemy zwrócić true/false

    public void introduceYourSelf() {
        System.out.println("Hi! My name is " + firstName + " " + lastName + " " + email);
    }

    public Boolean isAdult() {
        if (age > 18) {
            return true;
        } else {
            return false;
        }
    }

    public void metodaZParametrem(String parametr) {
        System.out.println("Parametr metody: " + parametr);
    }

    public void metodaZParametrami(String parametr1, String parametr2) {
        System.out.println("Parametr metody: " + parametr1 + " parametr drugi " + parametr2);
    }

    public void metodaZParametrem(String parametr1, String parametr2) {
        System.out.println("Parametr metody: " + parametr1 + " parametr drugi " + parametr2);
    }

    public void metodaZParametrem(String parametr1, Boolean parametr2) {
        System.out.println("Parametr metody: " + parametr1 + " parametr drugi " + parametr2);
    }

    //4. przeciązanie metody - dwie takie same metody z taką samą nazwą ale różnią się liczbą parametrami, zwracany typ jest taki sam


    //6. Enkapsulacja - ukrywanie danych poprzez przypisanie prywatnego modyfikatora dostępu
    //Nigdy nie udostępniamy pól ustawiając im modyfikator public po to tworzymy gettery i settery
    //getter - odczytuje dane
    //setter - modyfikuje dane
}
